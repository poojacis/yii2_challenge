<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Oadode */
/* @var $form ActiveForm */
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<div class="create">

    <?php $form = ActiveForm::begin(); ?>

        
        <?= $form->field($model, 'legal_name') ?>
        <?= $form->field($model, 'business_name') ?>
        <?= $form->field($model, 'business_address') ?>
        <?= $form->field($model, 'business_mailing_address') ?>
        <?= $form->field($model, 'business_phone') ?>
        <?= $form->field($model, 'business_fax') ?>
        <?= $form->field($model, 'business_email') ?>
        <div class="form-group">
        <table>
        <tr>
            <th>Description of Controlled Goods</th>
            <th>ECL Group No.</th>
            <th>Ecl Item No.</th>
        </tr>
        <?php foreach($goodsModels as $i=>$goodsModel): ?>
        <tr>
        <td><?php echo $form->field($goodsModel, "[$i]description")->label(false);  ?></td>
        <td><?php echo $form->field($goodsModel, "[$i]ecl_group")->label(false); ?></td>
        <td><?php echo $form->field($goodsModel, "[$i]ecl_item")->label(false); ?></td>
        </tr>
        <?php endforeach; ?>
        </table>

        
        </div>

        <?= $form->field($model, 'application_type')->checkboxList($model->applicationTypeArr,['class' => 'app_type']) ?>
        <?= $form->field($model, 'business_title')->checkboxList($model->businessTitleArr) ?>
        <?= $form->field($model, 'lang')->checkboxList($model->langPreferredArr,['class' => 'lang']) ?>

        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- create -->

<script>
$( '.app_type' ).on( 'click', ':checkbox', function( e ) { // attach click event
    var is_checked = $(e.currentTarget).is(':checked')
    $('.app_type input:checkbox'). prop('checked', false);
    if(is_checked){
        $(this).prop( "checked", true );
    }
  } );

  $( '.lang' ).on( 'click', ':checkbox', function( e ) { // attach click event
    var is_checked = $(e.currentTarget).is(':checked')
    $('.lang input:checkbox'). prop('checked', false);
    if(is_checked){
        $(this).prop( "checked", true );
    }
  } );
</script>