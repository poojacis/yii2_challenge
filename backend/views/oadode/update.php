<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Oadode */

$this->title = 'Update Oadode: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Oadodes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="oadode-update">

    <h1><?= Html::encode($this->title) ?></h1>

  


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<div class="-sec">
			<div class="heading-form text-left">
				<h2>A. Business Information (to be completed by the Designated Official)</h2>
			</div>

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
		<div class="form-group form-field col-md-12">
            <?= $form->field($model, 'legal_name') ?>
        </div>
        <div class="form-group form-field col-md-12">
            <?= $form->field($model, 'business_name') ?>
        </div>
        <div class="form-group form-field col-md-12">
            <?= $form->field($model, 'business_address') ?>
        </div>
        <div class="form-group form-field col-md-12">
            <?= $form->field($model, 'business_mailing_address') ?>
        </div>
        <div class="form-group form-field col-md-6">
            <?= $form->field($model, 'business_phone') ?>
        </div>
        <div class="form-group form-field col-md-6">
            <?= $form->field($model, 'business_fax') ?>
        </div>
        <div class="form-group form-field col-md-12">
            <?= $form->field($model, 'business_email') ?>
        </div>

        <div class="form-group form-field col-md-12">
        <label for="">Descripation of the controlled goods the applicant may be required to examine, possess or transfer <span>(Refer to the Export Control List (ECL))</span></label>
        <table class="table table-form">
            <thead>
                <tr>
                <th colspan="2" class="text-center" style="width: 60%">Description of controlled Goods</th>
                <th class="text-center" style="width: 20%">ECL Group No.</th>
                <th class="text-center" style="width: 20%">ECL Item No.</th>
                </tr>
            </thead>

            <tbody>
                <?php foreach($goodsModels as $i=>$goodsModel): ?>
                <tr>
                    <td><?= $i+1 ?></td>
                    <td><?php echo $form->field($goodsModel, "[$i]description")->label(false);  ?></td>
                    <td><?php echo $form->field($goodsModel, "[$i]ecl_group")->label(false); ?></td>
                    <td><?php echo $form->field($goodsModel, "[$i]ecl_item")->label(false); ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

        
        </div>
        <div class="heading-form text-left col-md-12">
            <h2>B. Appplicant Information (To be completed by the applicant)</h2>
        </div>

        <div class="form-group checkbox-form-group checkbox-form-inline-group form-sec-no-m col-md-12 inline-form-span">
                    <?= $form->field($model, 'application_type')->checkboxList($model->applicationTypeArr, ['class' => 'app_type',
                        'item' => function($index, $label, $name, $checked, $value) {
                        return "<label><input type='checkbox' {$checked} name='{$name}' value='{$value}' tabindex='3'>{$label}<span class='checkmark'></span></label>";}
        ])?>
        </div>
        <div class="form-group checkbox-form-group checkbox-form-inline-group form-sec-no-m col-md-12 inline-form-span">
                    <?= $form->field($model, 'business_title')->checkboxList($model->businessTitleArr, [
                        'item' => function($index, $label, $name, $checked, $value) {
                        return "<label><input type='checkbox' {$checked} name='{$name}' value='{$value}' tabindex='3'>{$label}<span class='checkmark'></span></label>";}
        ])?>
        </div>
        <div class="form-group checkbox-form-group checkbox-form-inline-group form-sec-no-m col-md-12 inline-form-span">
                    <?= $form->field($model, 'lang')->checkboxList($model->langPreferredArr, ['class' => 'lang',
                        'item' => function($index, $label, $name, $checked, $value) {
                        return "<label><input type='checkbox' {$checked} name='{$name}' value='{$value}' tabindex='3'>{$label}<span class='checkmark'></span></label>";}
        ])?>
        </div>

        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- create -->
</div>
<script>
$( '.app_type' ).on( 'click', ':checkbox', function( e ) { // attach click event
    var is_checked = $(e.currentTarget).is(':checked')
    $('.app_type input:checkbox'). prop('checked', false);
    if(is_checked){
        $(this).prop( "checked", true );
    }
  } );

  $( '.lang' ).on( 'click', ':checkbox', function( e ) { // attach click event
    var is_checked = $(e.currentTarget).is(':checked')
    $('.lang input:checkbox'). prop('checked', false);
    if(is_checked){
        $(this).prop( "checked", true );
    }
  } );
</script>

