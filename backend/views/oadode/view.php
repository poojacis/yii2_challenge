<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Oadode */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Oadodes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="oadode-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Print',['print', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>


<table class="table table-form" style="width: 100%;max-width: 1000px;margin: 0 auto; box-shadow: 0 0 5px #000; padding: 10px;">
		<tr style="width: 100%">
			<td style="width: 100%">
				<h2 style="font-weight: 700;color: #000;font-size: 20px;margin-bottom: 15px;font-family:Arial, Helvetica, sans-serif;">A. Business Information (to be completed by the Designated Official)</h2>
			</td>
		</tr>
		<tr style="width: 100%">
			<td style="width: 100%;font-family:Arial, Helvetica, sans-serif;">
				<label style="color: #000000;font-weight: 700;font-size: 12px;padding-bottom: 6px;width: 100%; display: block;font-family:Arial, Helvetica, sans-serif;"><span style="display:inline-block;border: 2px solid;padding: 7px 3px;line-height: 0;font-size: 13px !important;font-weight: 700;margin-right: 8px;">1</span>Legal Name :</label>
				<input value="<?php echo $model->legal_name?>" style="width: 99%; font-size: 12px;padding: 3px;margin-bottom: 12px;" type="text" name="Legal">
			</td>
		</tr>
		<tr style="width: 100%">
			<td style="width: 100%;font-family:Arial, Helvetica, sans-serif;">
				<label style="color: #000000;font-weight: 700;font-size: 12px;padding-bottom: 6px;width: 100%;display: block;font-family:Arial, Helvetica, sans-serif;"><span style="display:inline-block;border: 2px solid;padding: 7px 3px;line-height: 0;font-size: 13px !important;font-weight: 700;margin-right: 8px;">2</span>Business Name (If different from lehal name) :</label>
				<input value="<?php echo $model->business_name?>" style="width: 99%;font-size: 12px;padding: 3px;margin-bottom: 12px;" type="text" name="Business">
			</td>
		</tr>
		<tr style="width: 100%">
			<td style="width: 100%;font-family:Arial, Helvetica, sans-serif;">
				<label style="color: #000000;font-weight: 700;font-size: 12px;padding-bottom: 6px;width: 100%;display: block;font-family:Arial, Helvetica, sans-serif;"><span style="display:inline-block;border: 2px solid;padding: 7px 3px;line-height: 0;font-size: 13px !important;font-weight: 700;margin-right: 8px;">3</span>Civic Address :</label>
				<input value="<?php echo $model->business_address?>" style="width: 99%;font-size: 12px;padding: 3px;margin-bottom: 12px;" type="text" name="Civic">
			</td>
		</tr>
		<tr style="width: 100%">
			<td style="width: 100%;font-family:Arial, Helvetica, sans-serif;">
				<label style="color: #000000;font-weight: 700;font-size: 12px;padding-bottom: 6px;width: 100%;display: block;font-family:Arial, Helvetica, sans-serif;"><span style="display:inline-block;border: 2px solid;padding: 7px 3px;line-height: 0;font-size: 13px !important;font-weight: 700;margin-right: 8px;">4</span>Mailling Address (If different from civic address) :</label>
				<input value="<?php echo $model->business_mailing_address?>" style="width: 99%;font-size: 12px;padding: 3px;margin-bottom: 12px;" type="text" name="Mailling">
			</td>
		</tr>
		<tr style="width: 100%">
			<td style="width: 100%">
				<table style="width: 100%;margin: 0;padding: 0;">
                    <tr style="width: 100%">
                    
                       
						<td style="width: 49%;padding-right: 6px;display: inline-block;font-family:Arial, Helvetica, sans-serif;">
							<label style="color: #000000;font-weight: 700;font-size: 12px;padding-bottom: 6px;width: 100%;display: block;font-family:Arial, Helvetica, sans-serif;"><span style="display:inline-block;border: 2px solid;padding: 7px 3px;line-height: 0;font-size: 13px !important;font-weight: 700;margin-right: 8px;">5</span>Facsimile Number :</label>
							<input value="<?php echo $model->business_fax?>" style="width: 99%;font-size: 12px;padding: 3px;margin-bottom: 12px;" type="text" name="Facsimile">
						</td>
						<td style="width: 49%;padding-left: 6px;display: inline-block;font-family:Arial, Helvetica, sans-serif;">
							<label style="color: #000000;font-weight: 700;font-size: 12px;padding-bottom: 6px;width: 100%;display: block;font-family:Arial, Helvetica, sans-serif;"><span style="display:inline-block;border: 2px solid;padding: 7px 3px;line-height: 0;font-size: 13px !important;font-weight: 700;margin-right: 8px;">6</span>Telephone Number (Include extension no. if applicable) :</label>
							<input value="<?php echo $model->business_phone?>" style="width: 99%;font-size: 12px;padding: 3px;margin-bottom: 12px;" type="text" name="Telephone">
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr style="width: 100%">
			<td style="width: 100%;font-family:Arial, Helvetica, sans-serif;">
				<label style="color: #000000;font-weight: 700;font-size: 12px;padding-bottom: 6px;width: 100%;display: block;font-family:Arial, Helvetica, sans-serif;"><span style="display:inline-block;border: 2px solid;padding: 7px 3px;line-height: 0;font-size: 13px !important;font-weight: 700;margin-right: 8px;">7</span>E-mail :</label>
				<input value="<?php echo $model->business_email?>" style="width: 99%;font-size: 12px;padding: 3px;margin-bottom: 12px;" type="text" name="Email">
			</td>
		</tr>
		<tr style="width: 100%">
			<td style="width: 100%;font-family:Arial, Helvetica, sans-serif;">
				<label style="color: #000000;font-weight: 700;font-size: 12px;padding-bottom: 6px;width: 100%;display: block;font-family:Arial, Helvetica, sans-serif;"><span style="display:inline-block;border: 2px solid;padding: 7px 3px;line-height: 0;font-size: 13px !important;font-weight: 700;margin-right: 8px;">8</span>Descripation of the controlled goods the applicant may be required to examine, possess or transfer <span>(Refer to the Export Control List (ECL))</label>
			</td>
		</tr>
		<tr style="width: 100%">
			<td style="width: 100%;font-family:Arial, Helvetica, sans-serif;font-family:Arial, Helvetica, sans-serif;">
				<table style="width: 100%" cellspacing="0" cellpadding="0">
					<tr style="width: 100%">
						<th colspan="2" style="border: 1px solid black;border-right: none;padding: 3px;width: 60%;font-family:Arial, Helvetica, sans-serif;">Description of controlled Goods</th>
						<th style="border: 1px solid black;border-right: none;padding: 3px;width: 20%;font-family:Arial, Helvetica, sans-serif;">ECL Group No.</th>
						<th style="border: 1px solid black;padding: 3px;width: 20%;font-family:Arial, Helvetica, sans-serif;">ECL Item No.</th>
                    </tr>
                   
                    <?php foreach($goods as $good){?>
					<tr style="width: 100%">
						<td style="border: 1px solid black;padding: 3px;border-top: none;width: 4%;text-align: center;font-weight: bold;font-family:Arial, Helvetica, sans-serif;"></td>
						<td style="border: 1px solid black;padding: 3px;border-top: none;width: 56%;border-left: none;border-right: none;font-family:Arial, Helvetica, sans-serif;"><?php echo $good->description?></td>
						<td style="border: 1px solid black;padding: 3px;border-top: none;border-right: none;font-family:Arial, Helvetica, sans-serif;"><?php echo $good->ecl_group?></td>
						<td style="border: 1px solid black;padding: 3px;border-top: none;font-family:Arial, Helvetica, sans-serif;"><?php echo $good->ecl_item?></td>
                    </tr>
                    <?php } ?>
				
				</table>
			</td>
		</tr>
		<tr style="width: 100%">
			<td style="width: 100%;font-family:Arial, Helvetica, sans-serif;">
				<h2 style="font-weight: 700;color: #000;font-size: 20px;margin-bottom: 15px;font-family:Arial, Helvetica, sans-serif;">B. Appplicant Information (To be completed by the applicant)</h2>
			</td>
		</tr>
		<tr style="width: 100%">
			<td style="width: 100%">
				<table style="width: 100%;margin-bottom: 15px;">
					<tr style="width: 100%">
						<td style="width: 50%;font-family:Arial, Helvetica, sans-serif;">
							<label style="color: #000000;font-weight: 700;font-size: 12px;padding-bottom: 6px;width: 100%;display: block;font-family:Arial, Helvetica, sans-serif;"><span style="display:inline-block;border: 2px solid;padding: 7px 3px;line-height: 0;font-size: 13px !important;font-weight: 700;margin-right: 8px;">9</span>Type of Application</label>
                        </td>
                        <?php foreach($model->applicationTypeArr as $k=>$app_type){?>
						<td style="color: #000000;font-weight: 700;font-size: 12px;width: 10%;font-family:Arial, Helvetica, sans-serif;">
							<input type="checkbox" <?php echo  ($k==$model->application_type)?"checked":""?> name="" value="New"><?php echo $app_type?>
                        </td>
                        <?php } ?>
					
					</tr>
				</table>
			</td>
		</tr>

		<tr style="width: 100%">
			<td style="width: 100%">
				<table style="width: 100%;margin-bottom: 15px;">
					<tr style="width: 100%" >
						<td style="width: 100%;font-family:Arial, Helvetica, sans-serif;" colspan="6">
							<label style="color: #000000;font-weight: 700;font-size: 12px;padding-bottom: 6px;width: 100%;display: block;font-family:Arial, Helvetica, sans-serif;"><span style="display:inline-block;border: 2px solid;padding: 7px 3px;line-height: 0;font-size: 13px !important;font-weight: 700;margin-right: 8px;">10</span>Business Title (Select all that apply) </label>
						</td>
					</tr>
                    <tr style="width: 100%;font-family:Arial, Helvetica, sans-serif;">
                        <?php foreach($model->businessTitleArr as $k=>$business_type){?>
						<td style="color: #000000;font-weight: 700;font-size: 12px;font-family:Arial, Helvetica, sans-serif;">
                        <input type="checkbox" <?php echo  ($k==$model->business_title)?"checked":""?> name="" value="New"><?php echo $business_type?>
                        </td>
                        <?php ?>
						<?php } ?>
					</tr>
				</table>
			</td>
		</tr>


		<tr style="width: 100%">
			<td style="width: 100%">
				<table style="width: 100%">
					<tr style="width: 100%">
						<td style="width: 50%;font-family:Arial, Helvetica, sans-serif;">
							<label style="color: #000000;font-weight: 700;font-size: 12px;padding-bottom: 6px;width: 100%;display: block;font-family:Arial, Helvetica, sans-serif;"><span style="display:inline-block;border: 2px solid;padding: 7px 3px;line-height: 0;font-size: 13px !important;font-weight: 700;margin-right: 8px;">11</span>Preferred Language of Correspondence</label>
						</td>
                        <?php foreach($model->langPreferredArr as $k=>$lang){?>
						<td style="color: #000000;font-weight: 700;font-size: 12px;width: 10%;font-family:Arial, Helvetica, sans-serif;">
							<input type="checkbox" <?php echo  ($k==$model->lang)?"checked":""?> name="" value="New"><?php echo $lang?>
                        </td>
                        <?php } ?>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</div>
