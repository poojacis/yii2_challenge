<?php

namespace backend\controllers;

use Yii;
use common\models\Oadode;
use common\models\OadodeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\DescriptionOfGoods;
use kartik\mpdf\Pdf;
/**
 * OadodeController implements the CRUD actions for Oadode model.
 */
class OadodeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Oadode models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OadodeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Oadode model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        $goods = DescriptionOfGoods::findAll(array('application_id'=>$id));
        return $this->render('view', [
            'model' => $this->findModel($id),
            'goods'=>$goods
        ]);
    }

    /**
     * Creates a new Oadode model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Oadode;
        $goodsModels = [new DescriptionOfGoods()];
        for($i = 1; $i < 5; $i++) {
            $goodsModels[] = new DescriptionOfGoods();
        }

        if (Yii::$app->request->isPost){
          
            $postData = Yii::$app->request->post();
            $model->load($postData);
          
            $model->application_type =  !empty($postData['Oadode']['application_type'])?$postData['Oadode']['application_type'][0]:"";
            $model->business_title =  !empty($postData['Oadode']['business_title'])?implode(",",$postData['Oadode']['business_title']):"";
            $model->lang =  !empty($postData['Oadode']['lang'])?$postData['Oadode']['lang'][0]:"";
            if($model->validate()){
                $all_models = DescriptionOfGoods::loadMultiple($goodsModels, Yii::$app->request->post());
            
                $validate = true;
                foreach ($goodsModels as $goodsModel) {
                        if(!$goodsModel->validate()){
                            $validate = false;
                            break;
                        }
                }

                if($validate){
                    $model->save();
                    foreach ($goodsModels as $goodsModel) {
                        $goodsModel->application_id = $model->id;
                        $goodsModel->save();
                    }
                    return $this->redirect(['index']);

                }
               
            }
           
        }
       
        
       // $this->addError($attribute_name, Yii::t('user', 'At least 1 of the field must be filled up properly'));
        return $this->render('create',compact('model','goodsModels'));
    }

    /**
     * Updates an existing Oadode model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $model->application_type= explode(",",$model->application_type);
        $model->lang= explode(",",$model->lang);
        $model->business_title= explode(",",$model->business_title);

        $goodsModels = DescriptionOfGoods::findAll(array('application_id'=>$id));

        if (Yii::$app->request->isPost){
            $postData = Yii::$app->request->post();
            $model->load($postData);
          
            $model->application_type =  !empty($postData['Oadode']['application_type'])?$postData['Oadode']['application_type'][0]:"";
            $model->business_title =  !empty($postData['Oadode']['business_title'])?implode(",",$postData['Oadode']['business_title']):"";
            $model->lang =  !empty($postData['Oadode']['lang'])?$postData['Oadode']['lang'][0]:"";
            if($model->validate()){
                $all_models = DescriptionOfGoods::loadMultiple($goodsModels, Yii::$app->request->post());
            
                $validate = true;
                foreach ($goodsModels as $goodsModel) {

                        if(!$goodsModel->validate()){
                            $validate = false;
                            break;
                        }
                }

                if($validate){
                    $model->save();
                    DescriptionOfGoods::deleteAll(array('application_id'=>$id));
                    foreach ($goodsModels as $goodsModel) {
                        $goodsModel->application_id = $model->id;
                        if($goodsModel->save()){
                            
                        }else{
                            echo "<pre>";
                            print_r($goodsModel->getErrors());
                            exit;
                        }
                    }
                    return $this->redirect(['index']);

                }
               
            }
        }

        return $this->render('update',compact('model','goodsModels'));
    }

    /**
     * Deletes an existing Oadode model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Oadode model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Oadode the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Oadode::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionPrint($id){
        $goods = DescriptionOfGoods::findAll(array('application_id'=>$id));
        $model = $this->findModel($id);
        $content = $this->renderPartial('_print',compact('model','goods'));
    
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_CORE, 
            // A4 paper format
            'format' => Pdf::FORMAT_A4, 
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT, 
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER, 
            // your html content input
            'content' => $content,  
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting 
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}', 
            // set mPDF properties on the fly
            'options' => ['title' => 'Krajee Report Title'],
            // call mPDF methods on the fly
            'methods' => [ 
                'SetHeader'=>['Krajee Report Header'], 
                'SetFooter'=>['{PAGENO}'],
            ]
        ]);
        
        // return the pdf output as per the destination setting
        return $pdf->render(); 
    }
}
