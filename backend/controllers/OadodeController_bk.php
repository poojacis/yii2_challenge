<?php

namespace backend\controllers;

use Yii;
use common\models\Oadode;
use common\models\DescriptionOfGoods;
class OadodeController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCreate()
    {
        $model = new Oadode;
        $goodsModels = [new DescriptionOfGoods()];
        for($i = 1; $i < 5; $i++) {
            $goodsModels[] = new DescriptionOfGoods();
        }

        if (Yii::$app->request->isPost){
          
            $postData = Yii::$app->request->post();
            $model->load($postData);
          
            $model->application_type =  !empty($postData['Oadode']['application_type'])?$postData['Oadode']['application_type'][0]:"";
            $model->business_title =  !empty($postData['Oadode']['business_title'])?implode(",",$postData['Oadode']['business_title']):"";
            $model->lang =  !empty($postData['Oadode']['lang'])?$postData['Oadode']['lang'][0]:"";
            if($model->validate()){
                $all_models = DescriptionOfGoods::loadMultiple($goodsModels, Yii::$app->request->post());
            
                $validate = true;
                foreach ($goodsModels as $goodsModel) {
                        if(!$goodsModel->validate()){
                            $validate = false;
                        }
                }

                if($validate){
                    $model->save();
                    foreach ($goodsModels as $goodsModel) {
                        $goodsModel->application_id = $model->id;
                        $goodsModel->save();
                    }

                }
               
            }
           
        }
       
        
       // $this->addError($attribute_name, Yii::t('user', 'At least 1 of the field must be filled up properly'));
        return $this->render('create',compact('model','goodsModels'));
    }

}
